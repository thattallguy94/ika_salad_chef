# IKA_Salad_Chef

Salad chef coop game simulation.Currently supports only 2 chefs, although provision is made to support more than 2.


Player 1 controls (Up = UpArrow , Down = DownArrow , Left = LeftArrow , Right = RightArrow , Pickup = Enter , Drop = Del)


Player 2 controls (Up = W , Down = S , Left = A , Right = D , Pickup = G , Drop = F)


These controls can be altered by changing the values in the respective chef prefabs in scene hierarchy (Hierarchy : GameScene>Prefabs)


i have used json and json.net to save the game details as unitys json utility is not upto the mark.i use json in conjuction with playerprefs to create savegame


Also in this project found in the support folder are some of the Utility scripts that i have developed for my personal use for ease of programming(i developed these over the course of the many unity projects i worked on)


All the important game values can be found in the GameController script attached to the Controller game object in the hierarchy.You can tweak them through the editor


Calculation of timeleft variable in GenerateCombo function in Customer class still needs tweaking .Currently using some constants and random ranges.


Also for the purposes of this test i have programmed all the interaction in Unity UI(just easier to code thats all)


If i were to continue working on this project further , i would investigate why the collisions dont work if i make an exe of the build (it has something to do Unity UI(onpreliminary google search))


Afterwards i would change all the placeholder art to a good chef themed artwork.Add sounds and implement proper mainmenu and gameover screens




