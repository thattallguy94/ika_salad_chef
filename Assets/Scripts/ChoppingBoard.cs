﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ChoppingBoard : ChefInteractable
{
    public static float choppingTime = 3f;
    protected override void OnDrop(Chef chef)
    {
        var veg = chef.DropVeg();
        if (veg == null)
            return;
        chef.blockInput = true;
        var img = GetComponent<Image>();
        var color = GetComponent<Image>().color;

        img.DOColor(Color.green, choppingTime).OnComplete(() =>
        {

            var go = gameObject.AddChild(chef.PickedUpVegetablePrefab);
            go.FindComponentInAllChildren<Text>().text = veg;

            img.color = color;
            chef.blockInput = false;
        });


    }

    protected override void OnPickup(Chef chef)
    {
        if (chef.PickedVegs.Count > 0)
            return;

        var combo = "";
        foreach (Transform child in gameObject.transform)
        {
            combo += child.gameObject.FindComponentInAllChildren<Text>().text;
        }
        gameObject.DestroyChildren();
        chef.PickCombination(combo);
    }

}
