﻿using System;
using UnityEngine;

[Serializable]
public class ControlMap
{
    public KeyCode Up = KeyCode.UpArrow;
    public KeyCode Down = KeyCode.DownArrow;
    public KeyCode Left = KeyCode.LeftArrow;
    public KeyCode Right = KeyCode.RightArrow;
    public KeyCode Pickup = KeyCode.KeypadEnter;
    public KeyCode Drop = KeyCode.Delete;

}
