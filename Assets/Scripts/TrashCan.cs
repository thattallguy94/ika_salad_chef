﻿public class TrashCan : ChefInteractable
{
    protected override void OnDrop(Chef chef)
    {
        chef.DropCombination();
        chef.UpdateScore(-1 * GameController.Instance.TrashCanScoreDemerit);
    }
}
