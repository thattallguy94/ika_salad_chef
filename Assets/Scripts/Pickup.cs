﻿using DG.Tweening;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public int Recipient = 0;
    public int Type = -1;

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Chef"))
        {
            var chef = other.GetComponent<Chef>();
            if (Recipient == chef.GetInstanceID())
            {
                switch (Type)
                {
                    case 0://Speed pickup
                        {
                            //Bump speed to 1.8 times the actual speed then reverse it back after 10s
                            DOTween.Sequence().AppendInterval(0.1f)
                                .AppendCallback(() => chef.CurrentSpeed = chef.CurrentSpeed * GameController.Instance.PickUpSpeedBoostFactor).AppendInterval(10f)
                                .AppendCallback(() => chef.CurrentSpeed = GameController.Instance.ChefMovementSpeed).Play();
                            break;
                        }
                    case 1://Time pickup
                        {
                            chef.UpdateTime(GameController.Instance.TimePickupReward);
                            break;
                        }
                    case 2://Score pickup
                        {
                            chef.UpdateScore(GameController.Instance.ScorePickupReward);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }

            Destroy(gameObject);

        }
    }



}
