﻿using System;
using DG.Tweening;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class HighScoreContainer
{
    public List<KeyValuePair<string, int>> ScoreList = new List<KeyValuePair<string, int>>();
}

public class GameController : GenericSingleton<GameController>
{


    public float PickupCriteria = 0.7f;
    public float AvgWaitingTime = 10;
    public float ChefMovementSpeed = 30;
    public float PickUpSpeedBoostFactor = 1.8f;
    public float AngryCustomerTimeFactor = 0.5f;

    public int ChefDemeritScore = 125;
    public int ChefRewardScore = 1000;
    public int LevelTime = 120;
    public int TimePickupReward = 10;
    public int ScorePickupReward = 800;
    public int TrashCanScoreDemerit = 100;
    public int MaxComboLimit = 4;



    public RectTransform LevelArea;

    public GameObject VegPrefab;
    public GameObject VegCounter1;
    public GameObject VegCounter2;
    public GameObject HighScoreListPrefab;
    public GameObject PickupPrefab;
    public GameObject HighScoreListContainer;

    public List<Customer> CustomerCounters = new List<Customer>();
    public List<Chef> ChefPrefabs = new List<Chef>();
    public List<string> Vegetables;

    public UIPanel MainMenuScreen;
    public UIPanel GameOverScreen;
    public UIPanel MainGameScreen;
    public Text GameOverText;


    int MaxVegetables = 6;
    List<Chef> ChefsInLevel = new List<Chef>();


    public void OnClickPlay()
    {
        //as long as there are some chef prefabs loaded
        if (ChefPrefabs.Count > 0)
        {
            MainMenuScreen.DeactivatePanel();
            GameOverScreen.DeactivatePanel();
            MainGameScreen.ActivatePanel();
            InitVegCounter();
            SpawnChefs();
            InvokeRepeating("StartRestaurant", 0, 15);
            UpdateTime();

        }
    }
    public void OnClickRetry()
    {
        MainMenuScreen.ActivatePanel();
        GameOverScreen.DeactivatePanel();
        MainGameScreen.DeactivatePanel();
    }
    public void DemeritAllChefs(List<Chef> angerList)
    {
        ChefsInLevel.ForEach(x =>
        {
            if (!angerList.Contains(x))
                x.UpdateScore(-1 * ChefDemeritScore);
        });
    }
    void StartRestaurant()
    {
        var customers = CustomerCounters.Where(x => !x.isActive).PickRandom(Random.Range(1, 3)).ToList();
        foreach (var cust in customers)
        {
            cust.Init();
        }

    }
    void UpdateTime()
    {
        DOTween.Sequence().AppendCallback(() =>
        {
            ChefsInLevel.ForEach(x =>
            {
                x.UpdateTime(-1);

            });
            if (ChefsInLevel.Count(x => x.TimeLeft <= 0) == ChefsInLevel.Count)
            {
                GameOver();
            }
        }).AppendInterval(1f).SetLoops(-1).SetId("Timer").Play();
    }
    void GameOver()
    {
        DOTween.Kill("Timer");
        Debug.Log("Game Over");
        CancelInvoke();
        ChefsInLevel = ChefsInLevel.OrderByDescending(x => x.Score).ToList();
        GameOverText.text = string.Format("Game Over : Winner is {0} : Score {1}", ChefsInLevel[0].gameObject.name,
            ChefsInLevel[0].Score);
        DoHighScoreCalculations(ChefsInLevel[0].gameObject.name, ChefsInLevel[0].Score);
        DisplayHighScores();
        foreach (var chef in ChefsInLevel)
        {
            Destroy(chef);
        }
        ChefsInLevel.Clear();

        LevelArea.gameObject.DestroyChildren();
        MainMenuScreen.DeactivatePanel();
        GameOverScreen.ActivatePanel();
        MainGameScreen.DeactivatePanel();
    }
    void DisplayHighScores()
    {
        HighScoreListContainer.DestroyChildren();
        var highScores = new HighScoreContainer();
        var key = "HighScore";
        if (PlayerPrefs.HasKey(key))
            highScores = JsonConvert.DeserializeObject<HighScoreContainer>(PlayerPrefs.GetString(key));
        int i = 1;
        highScores.ScoreList.ForEach(x =>
        {
            var go = HighScoreListContainer.AddChild(HighScoreListPrefab);
            go.GetComponent<Text>().text = string.Format("{0}.[ {1} ] - {2} ", i, x.Key, x.Value);
            i++;
        });
    }
    void DoHighScoreCalculations(string playerName, int score)
    {
        var highScores = new HighScoreContainer();
        var key = "HighScore";
        if (PlayerPrefs.HasKey(key))
            highScores = JsonConvert.DeserializeObject<HighScoreContainer>(PlayerPrefs.GetString(key));


        highScores.ScoreList.Add(new KeyValuePair<string, int>(playerName, score));
        highScores.ScoreList = highScores.ScoreList.OrderByDescending(x => x.Value).ToList();
        highScores.ScoreList = highScores.ScoreList.Where(x => x.Value > 0).Take(10).ToList();

        PlayerPrefs.SetString(key, JsonConvert.SerializeObject(highScores));

    }
    void SpawnChefs()
    {
        //spawn the chef randombly in the play area
        var spawnArea = LevelArea.GetComponent<RectTransform>().rect.size;
        var prefab = ChefPrefabs[0].GetComponent<RectTransform>();
        var minX = -1 * ((spawnArea.x / 2) - (prefab.rect.width / 2));
        var maxX = ((spawnArea.x / 2) - (prefab.rect.width / 2));
        var minY = -1 * ((spawnArea.y / 2) - (prefab.rect.height / 2));
        var maxY = ((spawnArea.y / 2) - (prefab.rect.height / 2));

        for (int i = 0; i < ChefPrefabs.Count; i++)
        {
            var go = LevelArea.gameObject.AddChild(ChefPrefabs[i].gameObject);
            var chef = go.GetComponent<Chef>();
            var pos = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));

            go.GetComponent<RectTransform>().anchoredPosition = pos;
            go.Activate();

            chef.transform.parent = LevelArea.transform;
            chef.blockInput = false;
            chef.CurrentSpeed = ChefMovementSpeed;
            chef.TimeLeft = LevelTime;
            chef.UniqueId = Guid.NewGuid().ToString();
            ChefsInLevel.Add(chef);

        }
        Debug.Log("Chefs Spawned");

    }


    void InitVegCounter()
    {
        //init the vegetables from the vegetables list and add them to the counters 
        int i = 0;
        VegCounter1.DestroyChildren();
        VegCounter2.DestroyChildren();
        Vegetables.ForEach(x =>
        {
            if (i < MaxVegetables)
            {
                var go = (i < (MaxVegetables / 2) ? VegCounter1 : VegCounter2).AddChild(VegPrefab);
                go.FindComponentInAllChildren<Text>().text = x;
                go.GetComponent<BoxCollider2D>().size = go.GetComponent<RectTransform>().rect.size;
                i++;
            }

        });
    }

}
