﻿using UnityEngine.UI;

public class Vegetable : ChefInteractable
{

    protected override void OnPickup(Chef chef)
    {
        chef.AddVegetable(GetComponentInChildren<Text>().text);
    }

}
