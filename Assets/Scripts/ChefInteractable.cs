﻿
using UnityEngine;
using UnityEngine.UI;

public class ChefInteractable : MonoBehaviour
{

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Chef"))
        {
            var chef = other.GetComponent<Chef>();
            if (!chef.isInteracting)
            {
                chef.isInteracting = !chef.isInteracting;
                chef.OnPickup -= OnPickup;
                chef.OnPickup += OnPickup;

                chef.OnDrop -= OnDrop;
                chef.OnDrop += OnDrop;

                var img = GetComponent<Image>();
                img.color = new Color(img.color.r, img.color.g, img.color.b, 0.1f);
            }

        }
    }

    protected virtual void OnPickup(Chef chef)
    {

    }
    protected virtual void OnDrop(Chef chef)
    {

    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Chef"))
        {
            var chef = other.GetComponent<Chef>();
            chef.OnPickup -= OnPickup;
            chef.OnDrop -= OnDrop;
            chef.isInteracting = false;
            var img = GetComponent<Image>();
            img.color = new Color(img.color.r, img.color.g, img.color.b, 0.5f);
        }
    }

    public void Reset()
    {
        gameObject.DestroyChildren();
    }
}
