﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using System;


public static class ComponentExtensions
{


    public static void Deactivate(this UnityEngine.Object obj)
    {
        if (obj is GameObject)
        {
            ((GameObject)obj).SetActive(false);
        }
        else if (obj is Component)
        {
            if (((Component)obj).gameObject)
            {
                ((Component)obj).gameObject.SetActive(false);
            }
        }
        else if (obj is Graphic)
        {
            if (((Graphic)obj).gameObject)
            {
                ((Graphic)obj).gameObject.SetActive(false);
            }
        }
        else
        {
            throw new Exception("This object has to be a GameObject or a Component!");
        }
    }

    public static void Activate(this UnityEngine.Object obj)
    {
        if (obj is GameObject)
        {
            ((GameObject)obj).SetActive(true);
        }
        else if (obj is Component)
        {
            if (((Component)obj).gameObject)
            {
                ((Component)obj).gameObject.SetActive(true);
            }
        }
        else
        {
            throw new Exception("This object has to be a GameObject or a Component!");
        }
    }

    public static GameObject AddChild(this GameObject parent, GameObject prefab, bool worldPositionStays = false)
    {
        GameObject go = GameObject.Instantiate(prefab) as GameObject;
#if UNITY_EDITOR
        UnityEditor.Undo.RegisterCreatedObjectUndo(go, "Create Object");
#endif
        if (go != null && parent != null)
        {
            Transform t = go.transform;
            t.SetParent(parent.transform, worldPositionStays);
            t.localPosition = Vector3.zero;
            t.localRotation = Quaternion.identity;
            t.localScale = Vector3.one;
            go.layer = parent.layer;
            go.SetActive(true);
        }
        return go;
    }

    public static void DestroyChildren(this GameObject Parent)
    {
        var children = Parent.GetFirstLevelChildren();
        foreach (var item in children)
        {
            UnityEngine.Object.Destroy(item);
        }
    }
    public static List<GameObject> GetFirstLevelChildren(this GameObject obj)
    {
        List<GameObject> children = new List<GameObject>();
        foreach (Transform child in obj.transform)
        {
            children.Add(child.gameObject);
        }

        return children;
    }



    public static List<GameObject> GetAllChildren(this GameObject obj)
    {
        List<GameObject> children = new List<GameObject>();

        foreach (Transform child in obj.transform)
        {
            children.Add(child.gameObject);
            children.AddRange(GetAllChildren(child.gameObject));
        }

        return children;
    }


    public static T FindComponentInAllChildren<T>(this GameObject obj) where T : Component
    {
        var allChilds = obj.GetAllChildren();

        foreach (var child in allChilds)
        {
            T comp = child.GetComponent<T>();

            if (comp != null)
                return comp;
        }

        return null;
    }

}