using UnityEngine;

public abstract class GenericSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T _instance = null;
    protected static bool isSpawnedAlready = false;
    static GameObject _persistantContainer;
    static GameObject _nonPersistantContainer;

    protected virtual bool IsPersistant
    {
        get { return false; }
    }


    static GameObject PersistantContainer
    {
        get
        {
            if (_persistantContainer == null)
            {
                var container = GameObject.Find("PersistantSingletons");
                if (container == null)
                {
                    container = new GameObject("PersistantSingletons");
                    container.AddComponent<DontDestroyOnLoad>();
                }
                _persistantContainer = container;
            }
            return _persistantContainer;
        }
        set { _persistantContainer = value; }
    }

    static GameObject NonPersistantContainer
    {
        get
        {
            if (_nonPersistantContainer == null)
            {
                var container = GameObject.Find("NonPersistantSingletons");
                if (container == null)
                {
                    container = new GameObject("NonPersistantSingletons");
                }
                _nonPersistantContainer = container;
            }
            return _nonPersistantContainer;
        }
        set { _nonPersistantContainer = value; }
    }


    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType(typeof(T)) as T;
                if (_instance == null)
                {
                    _instance = new GameObject().AddComponent<T>();
                    _instance.gameObject.name = _instance.GetType().Name;

                    isSpawnedAlready = true;
                }
            }

            return _instance;
        }
        set { _instance = value; }
    }

    public static bool HasInstance
    {
        get { return !IsDestroyed; }
    }

    public static bool IsDestroyed
    {
        get { return _instance == null; }
    }


    protected virtual void Awake()
    {
        if (IsPersistant)
        {
            if (isSpawnedAlready)
            {
                Debug.Log("Deleting duplicate");
                Destroy(gameObject);
            }

            gameObject.transform.SetParent(PersistantContainer.transform);
            isSpawnedAlready = true;
        }
        else
        {
            gameObject.transform.SetParent(NonPersistantContainer.transform);
        }
    }

    protected virtual void OnApplicationQuit()
    {
        _instance = null;
    }
}