﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

public static class DelegatesExtensions
{

    public static void SafeInvoke(this Action callBack)
    {
        if (callBack != null)
        {
            callBack();
        }
    }

    public static void SafeInvoke(this UnityEvent callBack)
    {
        if (callBack != null)
        {
            callBack.Invoke();
        }
    }

   

}
