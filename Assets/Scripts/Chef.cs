﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chef : MonoBehaviour
{

    public float CurrentSpeed;
    public ControlMap Controls;
    public List<string> PickedVegs = new List<string>();

    public string VegCombo = "";
    public int Score = 0;
    public int TimeLeft = 0;
    public string UniqueId;

    public Action<Chef> OnPickup;
    public Action<Chef> OnDrop;

    public GameObject PickupContainer;
    public GameObject PickedUpVegetablePrefab;

    public bool isInteracting = false;
    public bool blockInput = false;

    float MoveTime = 0.1f;
    private RectTransform _rt;
    private RectTransform RectT
    {
        get
        {
            if (_rt == null)
                _rt = GetComponent<RectTransform>();

            return _rt;
        }
    }

    int MaxVegPickup = 2;

    public Text TimeText;
    public Text ScoreText;
    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;
        if (this.GetType() != obj.GetType()) return false;

        Chef p = (Chef)obj;
        return (this.UniqueId == p.UniqueId);
    }
    void Move()
    {
        var parentSize = RectT.parent.GetComponent<RectTransform>().rect.size;
        var minX = -1 * ((parentSize.x / 2) - (RectT.rect.width / 2));
        var maxX = ((parentSize.x / 2) - (RectT.rect.width / 2));
        var minY = -1 * ((parentSize.y / 2) - (RectT.rect.height / 2));
        var maxY = ((parentSize.y / 2) - (RectT.rect.height / 2));

        if (Input.GetKey(Controls.Up))
            RectT.DOLocalMoveY(Mathf.Clamp(RectT.anchoredPosition.y + CurrentSpeed, minY, maxY), MoveTime);
        else if (Input.GetKey(Controls.Down))
            RectT.DOLocalMoveY(Mathf.Clamp(RectT.anchoredPosition.y - CurrentSpeed, minY, maxY), MoveTime);
        else if (Input.GetKey(Controls.Left))
            RectT.DOLocalMoveX(Mathf.Clamp(RectT.anchoredPosition.x - CurrentSpeed, minX, maxX), MoveTime);
        else if (Input.GetKey(Controls.Right))
            RectT.DOLocalMoveX(Mathf.Clamp(RectT.anchoredPosition.x + CurrentSpeed, minX, maxX), MoveTime);

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (PickedVegs.Count == 2)
            return;
        if (other.CompareTag("Veg"))
            PickedVegs.Add(other.gameObject.FindComponentInAllChildren<Text>().text);
    }

    public void AddVegetable(string veg)
    {
        if (PickedVegs.Count == MaxVegPickup || !string.IsNullOrEmpty(VegCombo))
            return;
        var go = PickupContainer.AddChild(PickedUpVegetablePrefab);
        go.GetComponentInChildren<Text>().text = veg;
        PickedVegs.Add(veg);
    }
    public string DropVeg()
    {
        if (PickedVegs.Count > 0)
        {
            Destroy(PickupContainer.transform.GetChild(0).gameObject);
            var veg = PickedVegs[0];
            PickedVegs.RemoveAt(0);
            return veg;
        }
        return null;

    }
    public void PickCombination(string veg)
    {
        if (PickedVegs.Count > 0)
            return;

        PickupContainer.DestroyChildren();
        var go = PickupContainer.AddChild(PickedUpVegetablePrefab);
        go.GetComponentInChildren<Text>().text = veg;
        VegCombo = veg;

    }
    public string DropCombination()
    {
        if (!string.IsNullOrEmpty(VegCombo))
        {
            PickupContainer.DestroyChildren();
            var r = VegCombo;
            VegCombo = null;
            return r;
        }
        return null;

    }
    void Update()
    {
        if (blockInput)
            return;

        Move();
        if (Input.GetKeyDown(Controls.Pickup))
        {
            if (OnPickup != null)
                OnPickup.Invoke(this);
        }
        if (Input.GetKeyDown(Controls.Drop))
        {
            if (OnDrop != null)
                OnDrop.Invoke(this);
        }
    }

    public void UpdateTime(int t)
    {
        if (TimeLeft <= 0)
            return;

        TimeLeft += t;
        TimeText.text = string.Format("Time : {0}", TimeLeft);
    }
    public void UpdateScore(int score)
    {
        Score += score;

        if (Score <= 0)
            Score = 0;

        ScoreText.text = string.Format("Score : {0}", Score);
    }

    public void Reset()
    {
        TimeLeft = Score = 0;
        TimeText.text = string.Format("Time : {0}", TimeLeft);
        ScoreText.text = string.Format("Score : {0}", Score);
        VegCombo = null;
        PickedVegs.Clear();
        PickupContainer.DestroyChildren();
    }
}
