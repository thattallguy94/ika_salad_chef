﻿using UnityEngine.UI;

public class SidePlate : ChefInteractable
{

    protected override void OnDrop(Chef chef)
    {
        if (transform.childCount > 0)
            return;
        var veg = chef.DropVeg();
        if (veg != null)
        {
            var go = gameObject.AddChild(chef.PickedUpVegetablePrefab);
            go.FindComponentInAllChildren<Text>().text = veg;
        }

    }

    protected override void OnPickup(Chef chef)
    {
        if (transform.childCount > 0)
        {
            chef.AddVegetable(gameObject.GetComponentInChildren<Text>().text);
            gameObject.DestroyChildren();
        }

    }
}
