﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;

public class Customer : ChefInteractable
{
    public bool isActive = false;
    public bool isAngry = false;
    public Image waitingImage;
    public Text comboText;
   


    string Combo;
    float WaitingTime;
    double timeLeft;
    Tweener timeTill;
    List<Chef> AngerList = new List<Chef>();


    public void Init()
    {
        ResetCustomer();
        GenerateCombo();
        StartWaiting();
    }
    void GenerateCombo()
    {
        var combolist = GameController.Instance.Vegetables.PickRandom(UnityEngine.Random.Range(2, GameController.Instance.MaxComboLimit)).Shuffle().ToArray();
        Combo = string.Join("", combolist);
        timeLeft = WaitingTime = (combolist.Length * GameController.Instance.AvgWaitingTime) + UnityEngine.Random.Range(5, 20);
    }

    void StartWaiting()
    {
        isActive = true;
        comboText.text = Combo;
        waitingImage.DOColor(Color.red, WaitingTime).SetId("Color" + GetInstanceID());
        timeTill = waitingImage.DOFillAmount(0, WaitingTime).OnComplete(OnTimeUp);
    }

    bool CheckCombo(string combo)
    {
        return string.Equals(combo, Combo);
    }

    void UpdateTime()
    {
        waitingImage.DOKill();
        timeTill = waitingImage.DOFillAmount(0, (float)timeLeft * GameController.Instance.AngryCustomerTimeFactor).OnComplete(OnTimeUp);
        waitingImage.DOColor(Color.red, (float)timeLeft * GameController.Instance.AngryCustomerTimeFactor).SetId("Color" + GetInstanceID());
    }

    void OnTimeUp()
    {
        AngerList.ForEach(DemeritChef);

        GameController.Instance.DemeritAllChefs(AngerList);
        ResetCustomer();
    }
    void ResetCustomer()
    {
        comboText.text = null;
        waitingImage.color = Color.green;
        isActive = false;
        waitingImage.DOKill();
        waitingImage.DOFillAmount(1, 0);
        isAngry = false;
        AngerList.Clear();

    }

    void Update()
    {
        if (isActive)
            timeLeft = Math.Round(WaitingTime - timeTill.Elapsed(), 0);
    }
    protected override void OnDrop(Chef chef)
    {
        if (isActive)
        {
            var combo = chef.DropCombination();
            if (combo == null)
                return;
            if (CheckCombo(combo))
            {
                if (timeLeft > (WaitingTime * GameController.Instance.PickupCriteria))
                {
                    SpawnPickup(chef);
                }
                RewardChef(chef);
                ResetCustomer();
            }
            else
            {
                AngerList.Add(chef);
                UpdateTime();
            }
        }
    }

    void RewardChef(Chef chef)
    {
        Debug.Log("Chef Rewarded");
        chef.UpdateScore(GameController.Instance.ChefRewardScore);
    }
    void DemeritChef(Chef chef)
    {
        Debug.Log("Chef Demerited");
        chef.UpdateScore(-1 * GameController.Instance.ChefDemeritScore * 2);
    }


    void SpawnPickup(Chef chef)
    {
        Debug.Log("Pickup Spawned");
        var spawnArea = GameController.Instance.LevelArea.GetComponent<RectTransform>().rect.size;
        var prefab = GameController.Instance.PickupPrefab.GetComponent<RectTransform>();
        var minX = -1 * ((spawnArea.x / 2) - (prefab.rect.width / 2));
        var maxX = ((spawnArea.x / 2) - (prefab.rect.width / 2));
        var minY = -1 * ((spawnArea.y / 2) - (prefab.rect.height / 2));
        var maxY = ((spawnArea.y / 2) - (prefab.rect.height / 2));
        var pos = new Vector2(UnityEngine.Random.Range(minX, maxX), UnityEngine.Random.Range(minY, maxY));
        var go = GameController.Instance.LevelArea.gameObject.AddChild(prefab.gameObject);
        go.GetComponent<RectTransform>().anchoredPosition = pos;
        var reward = go.GetComponent<Pickup>();
        reward.Recipient = chef.GetInstanceID();
        reward.Type = UnityEngine.Random.Range(0, 3);
        var text = go.FindComponentInAllChildren<Text>();

        switch (reward.Type)
        {
            case 0:
                {
                    text.text = "Speed";
                    break;
                }
            case 1:
                {
                    text.text = "Time";
                    break;
                }
            case 2:
                {
                    text.text = "Score";
                    break;
                }
            default:
                {
                    break;
                }
        }


    }
}
